import {
  FindManyOptions,
  FindOneOptions,
  FindOptionsWhere,
  In,
  Repository,
  QueryRunner,
  DataSource,
  NoConnectionForRepositoryError } from 'typeorm';
import { DeleteResult } from 'typeorm/query-builder/result/DeleteResult';
import { UpdateResult } from 'typeorm/query-builder/result/UpdateResult';
import { UpdateResultModel } from '../models/updateResult.model';
import { Request } from 'express';
import {ModelFormatter} from "../formatters";

export abstract class AbstractRepository<T> {
  protected constructor(protected entity: Repository<T>,
                        protected request?: Request,
                        protected connection?: DataSource) {}

  async tx(
      fn: (queryRunner: QueryRunner, repo: Repository<T>) => void,
      replicaMode: 'master' | 'slave' = 'master',
  ) {
    const queryRunner = this.createQueryRunner(replicaMode, !!this.connection)

    if (!queryRunner) throw new NoConnectionForRepositoryError('QueryRunner');

    await queryRunner.startTransaction();
    try {
      await fn(queryRunner, queryRunner.manager.getRepository<T>(this.entity.target));
      await queryRunner.commitTransaction();
    } catch (err) {
      await queryRunner.rollbackTransaction();
      throw err;
    } finally {
      await queryRunner.release();
    }
  }

  createQueryRunner(replicaMode: 'master' | 'slave' = 'master', forTx = true): QueryRunner {
    return forTx ?
      this.connection.createQueryRunner(replicaMode) :
      this.entity.queryRunner;
  }

  getObjectForSearch(txRepo?: Repository<T>): Repository<T> {
    return txRepo ?? this.entity;
  }

  find(txRepo?: Repository<T>): Promise<T[]> {
    return (txRepo ?? this.entity).find();
  }

  findById(id: number, txRepo?: Repository<T>): Promise<T> {
    return this.getFindOne(this.getFindOptions({ id } as any), true, txRepo);
  }

  findByIds(ids: number[], txRepo?: Repository<T>): Promise<T[]> {
    return (txRepo ?? this.entity).find(this.getFindOptions({ id: In(ids) } as any));
  }

  save<C>(entity: C, txRepo?: Repository<T>): Promise<T> {
    return (txRepo ?? this.entity).save(entity as any);
  }

  update<C>(id: number, entity: Partial<C>, txRepo?: Repository<T>): Promise<UpdateResultModel<T>> {
    return (txRepo ?? this.entity)
      .createQueryBuilder()
      .update()
      .set({ ...entity } as any)
      .where('id = :id', {
        id,
      })
      .returning('*')
      .execute()
      .then(async response => {
        return {
          result: response.affected > 0,
          entity: await (txRepo ?? this.entity).findOneBy({ id } as any) as T
        };
      });
  }

  updateWithFormatter<C,M>(id: number, entity: Partial<C>, formatter: ModelFormatter<T>, formatterFunc?: (item: T) => M, txRepo?: Repository<T>): Promise<UpdateResultModel<M>> {
    return (txRepo ?? this.entity)
      .createQueryBuilder()
      .update()
      .set({ ...entity } as any)
      .where('id = :id', {
        id,
      })
      .returning('*')
      .execute()
      .then(async response => {
        return {
          result: response.affected > 0,
          entity: formatterFunc ? formatterFunc(
              await (txRepo ?? this.entity).findOneBy({ id } as any) as T
          ): formatter.formatItem(
              await (txRepo ?? this.entity).findOneBy({ id } as any) as T
          ),
        };
      });
  }

  async delete(id: number, txRepo?: Repository<T>): Promise<DeleteResult> {
    if (!(await (txRepo ?? this.entity).findOneByOrFail({ id } as any))) {
      return null;
    }
    return (txRepo ?? this.entity).delete(id);
  }

  async softDelete(id: number, txRepo?: Repository<T>): Promise<UpdateResult> {
    if (!(await (txRepo ?? this.entity).findOneByOrFail({ id } as any))) {
      return null;
    }
    return (txRepo ?? this.entity).softDelete(id);
  }

  protected getFindOne(
      options: FindOneOptions<T>,
      fail: boolean = false,
      txRepo?: Repository<T>,
  ): Promise<T | null>{
    return this.hasWithoutFailure() || !fail ? (txRepo ?? this.entity).findOne(options) : (txRepo ?? this.entity).findOneOrFail(options);
  }

  protected getFindOptions(
      where: FindOptionsWhere<T> | FindOptionsWhere<T>[],
  ): FindOneOptions<T> | FindManyOptions<T> {
    return {
      where,
      withDeleted: this.hasWithDeleted()
    };
  }

  protected hasWithDeleted() {
    return !!(this.request && this.request.query['with_deleted']);
  }

  protected hasWithoutFailure() {
    return !!(this.request && this.request.query['without_failure']);
  }
}
