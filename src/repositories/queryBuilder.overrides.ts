import {SelectQueryBuilder} from 'typeorm/query-builder/SelectQueryBuilder';
import {
    EntityMetadata,
    NoVersionOrUpdateDateColumnError,
    ObjectLiteral,
    OptimisticLockCanNotBeUsedError, OrderByCondition,
    PessimisticLockTransactionRequiredError,
    QueryRunner,
    TypeORMError,
} from 'typeorm';
import {SelectQuery} from 'typeorm/query-builder/SelectQuery';
import {DriverUtils} from 'typeorm/driver/DriverUtils';
import {ColumnMetadata} from 'typeorm/metadata/ColumnMetadata';
import {MysqlDriver} from 'typeorm/driver/mysql/MysqlDriver';
import {AuroraMysqlDriver} from 'typeorm/driver/aurora-mysql/AuroraMysqlDriver';
import {WhereClause} from 'typeorm/query-builder/WhereClause';
import {
    RelationIdMetadataToAttributeTransformer
} from 'typeorm/query-builder/relation-id/RelationIdMetadataToAttributeTransformer';
import {RelationCountLoader} from 'typeorm/query-builder/relation-count/RelationCountLoader';
import {RelationIdLoader} from 'typeorm/query-builder/relation-id/RelationIdLoader';
import {
    RelationCountMetadataToAttributeTransformer
} from 'typeorm/query-builder/relation-count/RelationCountMetadataToAttributeTransformer';
import {RawSqlResultsToEntityTransformer} from 'typeorm/query-builder/transformer/RawSqlResultsToEntityTransformer';
import {OrmUtils} from 'typeorm/util/OrmUtils';
import {snakeCase} from 'typeorm/util/StringUtils';

//Declaration Merging Of Module.
declare module 'typeorm/query-builder/SelectQueryBuilder' {
    interface SelectQueryBuilder<Entity> {
        initialQuery: string;
        mainQuery: string;

        prepareQueryForJoinsPagination(
            this: SelectQueryBuilder<Entity>,
        ): SelectQueryBuilder<Entity>;

        createInitialEntitySelectExpression(
            this: SelectQueryBuilder<Entity>,
            addOrder: boolean,
        ): string;

        createMainEntitySelectExpression(
            this: SelectQueryBuilder<Entity>,
            from: string,
            addOrder: boolean,
        ): string;

        publicCreateTableLockExpression(this: SelectQueryBuilder<Entity>): string;

        getEntityQuery(
            this: SelectQueryBuilder<Entity>,
            from: string,
            isInitialQuery: boolean,
        ): string;

        buildEscapedMainEntityColumnSelects(
            this: SelectQueryBuilder<Entity>,
            aliasName: string,
            metadata: EntityMetadata,
            skipInitialSelect: boolean,
        ): SelectQuery[];

        createMainEntityJoinExpression(this: SelectQueryBuilder<Entity>): string;

        createOneToOneJoinExpression(this: SelectQueryBuilder<Entity>): string;

        customCreateWhereExpression(
            this: SelectQueryBuilder<Entity>,
            isInitialQuery: boolean,
        ): string;

        customCreateWhereClausesExpression(
            this: SelectQueryBuilder<Entity>,
            clauses: WhereClause[],
            skipNonMainAliases: boolean,
        ): string;

        findAndReplaceMainAliasInWhereExpression(
            this: SelectQueryBuilder<Entity>,
            isInitialQuery: boolean,
            expression: string,
        ): string;

        customGetMany(this: SelectQueryBuilder<Entity>): Promise<Entity[]>;

        customGetRawAndEntities<T = any>(
            this: SelectQueryBuilder<Entity>,
        ): Promise<{
            entities: Entity[];
            raw: T[];
        }>;

        customExecuteEntitiesAndRawResults(
            this: SelectQueryBuilder<Entity>,
            queryRunner: QueryRunner,
        ): Promise<{ entities: Entity[]; raw: any[] }>;

        loadRawResultsPaginated(
            this: SelectQueryBuilder<Entity>,
            queryRunner: QueryRunner,
        ): Promise<Entity[]>;

        getDistinctIdsQuery(
            this: SelectQueryBuilder<Entity>,
        ): SelectQueryBuilder<any>;

        customReplacePropertyNames(
            this: SelectQueryBuilder<Entity>,
            statement: string,
        ): string;

        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        createOrderByCombinedWithSelectExpression(
            this: SelectQueryBuilder<Entity>,
            parentAlias: string,
        ): [string, OrderByCondition];

        camelToSnakeCase(this: SelectQueryBuilder<Entity>, str: string): string;
    }
}

SelectQueryBuilder.prototype.getEntityQuery = function <Entity>(
    this: SelectQueryBuilder<Entity>,
    from: string,
    isInitialQuery: boolean,
): string {
    let sql = this.createComment();
    sql += this.createCteExpression();
    if (isInitialQuery) {
        sql += this.createInitialEntitySelectExpression(true);
        sql += this.createOneToOneJoinExpression();
        sql += this.customCreateWhereExpression(true);
    } else {
        sql += this.createMainEntitySelectExpression(from, false);
        sql += this.createMainEntityJoinExpression();
        sql += this.customCreateWhereExpression(false);
    }
    sql += this.createGroupByExpression();
    sql += this.createHavingExpression();
    sql += this.createLockExpression();
    sql = sql.trim();
    if (this.expressionMap.subQuery) sql = '(' + sql + ')';
    return this.replacePropertyNamesForTheWholeQuery(sql);
};

SelectQueryBuilder.prototype.prepareQueryForJoinsPagination = function <Entity>(
    this: SelectQueryBuilder<Entity>,
): SelectQueryBuilder<Entity> {
    /* --- EXPECTED OUTCOME QUERY STRUCTURE
          WITH withInitialEntity AS (
              InitialQueryInitialQuery
          ),
           withMainQuery AS (
               MainQuery
           )
          SELECT DISTINCT "distinctAlias"."q_id", row_number
          FROM withMainQuery "distinctAlias"
          ORDER BY row_number asc
          LIMIT pagination
         */
    this.initialQuery = this.clone().getEntityQuery(null, true);
    this.mainQuery = this.clone().getEntityQuery('withinitialentity', false);

    return this;
};

/**
 * Creates "LOCK" part of SELECT Query after table Clause
 * ex.
 *  SELECT 1
 *  FROM USER U WITH (NOLOCK)
 *  JOIN ORDER O WITH (NOLOCK)
 *      ON U.ID=O.OrderID
 */
SelectQueryBuilder.prototype.publicCreateTableLockExpression = function <Entity>(
    this: SelectQueryBuilder<Entity>,
): string {
    if (this.connection.driver.options.type === 'mssql') {
        switch (this.expressionMap.lockMode) {
            case 'pessimistic_read':
                return ' WITH (HOLDLOCK, ROWLOCK)';
            case 'pessimistic_write':
                return ' WITH (UPDLOCK, ROWLOCK)';
            case 'dirty_read':
                return ' WITH (NOLOCK)';
        }
    }

    return '';
};

// Creates the query without the relations
SelectQueryBuilder.prototype.createInitialEntitySelectExpression = function <
    Entity
>(this: SelectQueryBuilder<Entity>, addOrder: boolean): string {
    if (!this.expressionMap.mainAlias)
        throw new TypeORMError(
            'Cannot build query because main alias is not set (call qb#from method)',
        );

    const allSelects: SelectQuery[] = [];
    const excludedSelects: SelectQuery[] = [];

    if (this.expressionMap.mainAlias.hasMetadata) {
        const metadata = this.expressionMap.mainAlias.metadata;
        allSelects.push(
            ...this.buildEscapedEntityColumnSelects(
                this.expressionMap.mainAlias.name,
                metadata,
            ),
        );
        excludedSelects.push(
            ...this.findEntityColumnSelects(
                this.expressionMap.mainAlias.name,
                metadata,
            ),
        );
    }

    // add selects from joins
    this.expressionMap.joinAttributes.forEach(join => {
        const relation = join.relation;
        let isDirectRelation = true;
        const parentAlias = join.parentAlias;
        if (
            relation?.relationType &&
            relation.relationType !== 'one-to-one' &&
            relation.relationType !== 'many-to-one'
        ) {
            isDirectRelation = false;
        }

        if (
            isDirectRelation &&
            parentAlias !== this.expressionMap.mainAlias!.name
        ) {
            isDirectRelation = false;
        }

        if (isDirectRelation && join.metadata) {
            allSelects.push(
                ...this.buildEscapedMainEntityColumnSelects(
                    join.alias.name!,
                    join.metadata,
                    false,
                ),
            );
            excludedSelects.push(
                ...this.findEntityColumnSelects(join.alias.name!, join.metadata),
            );
        }
    });

    // if still selection is empty, then simply set it to all (*)
    if (allSelects.length === 0) allSelects.push({selection: '*'});

    if (addOrder) {
        allSelects.push({
            selection: `ROW_NUMBER() OVER (${this.createOrderByExpression()})`,
            aliasName: 'row_number',
        });
    }

    // Use certain index
    let useIndex = '';
    if (this.expressionMap.useIndex) {
        if (DriverUtils.isMySQLFamily(this.connection.driver)) {
            useIndex = ` USE INDEX (${this.expressionMap.useIndex})`;
        }
    }

    // create a selection query
    const froms = this.expressionMap.aliases
        .filter(
            alias => alias.type === 'from' && (alias.tablePath || alias.subQuery),
        )
        .map(alias => {
            if (alias.subQuery) return alias.subQuery + ' ' + this.escape(alias.name);

            return (
                this.getTableName(alias.tablePath!) + ' ' + this.escape(alias.name)
            );
        });

    const select = this.createSelectDistinctExpression();
    const selection = allSelects
        .map(
            select =>
                select.selection +
                (select.aliasName ? ' AS ' + this.escape(select.aliasName) : ''),
        )
        .join(', ');

    return (
        select +
        selection +
        ' FROM ' +
        froms.join(', ') +
        this.publicCreateTableLockExpression() +
        useIndex
    );
};

// Creates the query fully but using
SelectQueryBuilder.prototype.createMainEntitySelectExpression = function <
    Entity
>(this: SelectQueryBuilder<Entity>, from: string, addOrder: boolean): string {
    if (!this.expressionMap.mainAlias)
        throw new TypeORMError(
            'Cannot build query because main alias is not set (call qb#from method)',
        );

    // todo throw exception if selects or from is missing

    const allSelects: SelectQuery[] = [];
    const excludedSelects: SelectQuery[] = [];

    if (this.expressionMap.mainAlias.hasMetadata) {
        const metadata = this.expressionMap.mainAlias.metadata;
        allSelects.push(
            ...this.buildEscapedMainEntityColumnSelects(
                this.expressionMap.mainAlias.name,
                metadata,
                true,
            ),
        );
        excludedSelects.push(
            ...this.findEntityColumnSelects(
                this.expressionMap.mainAlias.name,
                metadata,
            ),
        );
    }

    // add selects from joins
    this.expressionMap.joinAttributes.forEach(join => {
        if (join.metadata) {
            allSelects.push(
                ...this.buildEscapedMainEntityColumnSelects(
                    join.alias.name!,
                    join.metadata,
                    false,
                ),
            );
            excludedSelects.push(
                ...this.findEntityColumnSelects(join.alias.name!, join.metadata),
            );
        } else {
            const hasMainAlias = this.expressionMap.selects.some(
                select => select.selection === join.alias.name,
            );
            if (hasMainAlias) {
                allSelects.push({
                    selection: this.escape(join.alias.name!) + '.*',
                });
                const excludedSelect = this.expressionMap.selects.find(
                    select => select.selection === join.alias.name,
                );
                excludedSelects.push(excludedSelect!);
            }
        }
    });

    // add all other selects
    this.expressionMap.selects
        .filter(select => excludedSelects.indexOf(select) === -1)
        .filter(select => {
            let alias = this.customReplacePropertyNames(select.selection);
            alias = this.camelToSnakeCase(alias);
            return allSelects.findIndex(x => x.selection === alias) === -1;
        })
        .forEach(select =>
            allSelects.push({
                selection: this.customReplacePropertyNames(select.selection),
                aliasName: select.aliasName,
            }),
        );

    // if still selection is empty, then simply set it to all (*)
    if (allSelects.length === 0) allSelects.push({selection: '*'});

    allSelects.push({
        selection: `row_number`,
        aliasName: 'row_number',
    });

    // Use certain index
    let useIndex = '';
    if (this.expressionMap.useIndex) {
        if (DriverUtils.isMySQLFamily(this.connection.driver)) {
            useIndex = ` USE INDEX (${this.expressionMap.useIndex})`;
        }
    }

    const select = this.createSelectDistinctExpression();
    const selection = allSelects
        .map(
            select =>
                select.selection +
                (select.aliasName ? ' AS ' + this.escape(select.aliasName) : ''),
        )
        .join(', ');

    return (
        select +
        selection +
        ' FROM ' +
        from +
        this.publicCreateTableLockExpression() +
        useIndex
    );
};

SelectQueryBuilder.prototype.buildEscapedMainEntityColumnSelects = function <
    Entity
>(
    this: SelectQueryBuilder<Entity>,
    aliasName: string,
    metadata: EntityMetadata,
    skipInitialSelect: boolean,
): SelectQuery[] {
    const hasMainAlias = this.expressionMap.selects.some(
        select => select.selection === aliasName,
    );

    const columns: ColumnMetadata[] = [];
    if (hasMainAlias) {
        columns.push(
            ...metadata.columns.filter(column => column.isSelect === true),
        );
    }
    columns.push(
        ...metadata.columns.filter(column => {
            return this.expressionMap.selects.some(
                select => select.selection === aliasName + '.' + column.propertyPath,
            );
        }),
    );

    // if user used partial selection and did not select some primary columns which are required to be selected
    // we select those primary columns and mark them as "virtual". Later virtual column values will be removed from final entity
    // to make entity contain exactly what user selected
    if (columns.length === 0)
        // however not in the case when nothing (even partial) was selected from this target (for example joins without selection)
        return [];

    const nonSelectedPrimaryColumns = this.expressionMap.queryEntity
        ? metadata.primaryColumns.filter(
            primaryColumn => columns.indexOf(primaryColumn) === -1,
        )
        : [];
    const allColumns = [...columns, ...nonSelectedPrimaryColumns];

    const finalSelects: SelectQuery[] = [];
    allColumns.forEach(column => {
        let selectionPath =
            this.escape(aliasName) + '.' + this.escape(column.databaseName);
        if (this.connection.driver.spatialTypes.indexOf(column.type) !== -1) {
            if (
                DriverUtils.isMySQLFamily(this.connection.driver) ||
                this.connection.driver.options.type === 'aurora-mysql'
            ) {
                const useLegacy = (this.connection.driver as
                    | MysqlDriver
                    | AuroraMysqlDriver).options.legacySpatialSupport;
                const asText = useLegacy ? 'AsText' : 'ST_AsText';
                selectionPath = `${asText}(${selectionPath})`;
            }

            if (this.connection.driver.options.type === 'postgres')
                if (column.precision) {
                    // cast to JSON to trigger parsing in the driver
                    selectionPath = `ST_AsGeoJSON(${selectionPath}, ${column.precision})::json`;
                } else {
                    selectionPath = `ST_AsGeoJSON(${selectionPath})::json`;
                }
            if (this.connection.driver.options.type === 'mssql')
                selectionPath = `${selectionPath}.ToString()`;
        }

        const selections = this.expressionMap.selects.filter(
            select => select.selection === aliasName + '.' + column.propertyPath,
        );
        if (selections.length) {
            selections.forEach(selection => {
                const alias = selection.aliasName
                    ? selection.aliasName
                    : DriverUtils.buildAlias(
                        this.connection.driver,
                        undefined,
                        aliasName,
                        column.databaseName,
                    );
                finalSelects.push({
                    selection: skipInitialSelect ? alias : selectionPath,
                    aliasName: alias,
                    // todo: need to keep in mind that custom selection.aliasName breaks hydrator. fix it later!
                    virtual: selection.virtual,
                });
            });
        } else {
            const alias = DriverUtils.buildAlias(
                this.connection.driver,
                undefined,
                aliasName,
                column.databaseName,
            );

            finalSelects.push({
                selection: skipInitialSelect ? alias : selectionPath,
                aliasName: alias,
                // todo: need to keep in mind that custom selection.aliasName breaks hydrator. fix it later!
                virtual: hasMainAlias,
            });
        }
    });

    return finalSelects;
};

SelectQueryBuilder.prototype.createOneToOneJoinExpression = function <Entity>(
    this: SelectQueryBuilder<Entity>,
): string {
    const usedRelations = [];

    const joins = this.expressionMap.joinAttributes.map(joinAttr => {
        const relation = joinAttr.relation;
        const destinationTableName = joinAttr.tablePath;
        const destinationTableAlias = joinAttr.alias.name;
        let appendedCondition = joinAttr.condition
            ? ' AND (' + joinAttr.condition + ')'
            : '';
        const parentAlias = joinAttr.parentAlias;

        if (
            relation.relationType !== 'one-to-one' &&
            relation.relationType !== 'many-to-one'
        ) {
            return '';
        }

        if (
            parentAlias !== this.expressionMap.mainAlias!.name &&
            usedRelations.indexOf(parentAlias) === -1
        ) {
            return '';
        }

        usedRelations.push(parentAlias);

        // if join was build without relation (e.g. without "post.category") then it means that we have direct
        // table to join, without junction table involved. This means we simply join direct table.
        if (!parentAlias || !relation) {
            const destinationJoin = joinAttr.alias.subQuery
                ? joinAttr.alias.subQuery
                : this.getTableName(destinationTableName);
            return (
                ' ' +
                joinAttr.direction +
                ' JOIN ' +
                destinationJoin +
                ' ' +
                this.escape(destinationTableAlias) +
                this.publicCreateTableLockExpression() +
                (joinAttr.condition
                    ? ' ON ' + this.customReplacePropertyNames(joinAttr.condition)
                    : '')
            );
        }

        // if real entity relation is involved
        if (relation.isManyToOne || relation.isOneToOneOwner) {
            // JOIN `category` `category` ON `category`.`id` = `post`.`categoryId`
            const condition = relation.joinColumns
                .map(joinColumn => {
                    const result =
                        parentAlias +
                        '.' +
                        relation.propertyPath +
                        '.' +
                        joinColumn.referencedColumn!.propertyPath;

                    return (
                        destinationTableAlias +
                        '.' +
                        joinColumn.referencedColumn!.propertyPath +
                        '=' +
                        result
                    );
                })
                .join(' AND ');

            return (
                ' ' +
                joinAttr.direction +
                ' JOIN ' +
                this.getTableName(destinationTableName) +
                ' ' +
                this.escape(destinationTableAlias) +
                this.publicCreateTableLockExpression() +
                ' ON ' +
                this.customReplacePropertyNames(condition + appendedCondition)
            );
        } else if (relation.isOneToMany || relation.isOneToOneNotOwner) {
            // JOIN `post` `post` ON `post`.`categoryId` = `category`.`id`
            const condition = relation
                .inverseRelation!.joinColumns.map(joinColumn => {
                if (
                    relation.inverseEntityMetadata.tableType === 'entity-child' &&
                    relation.inverseEntityMetadata.discriminatorColumn
                ) {
                    appendedCondition +=
                        ' AND ' +
                        destinationTableAlias +
                        '.' +
                        relation.inverseEntityMetadata.discriminatorColumn.databaseName +
                        "='" +
                        relation.inverseEntityMetadata.discriminatorValue +
                        "'";
                }

                return (
                    destinationTableAlias +
                    '.' +
                    relation.inverseRelation!.propertyPath +
                    '.' +
                    joinColumn.referencedColumn!.propertyPath +
                    '=' +
                    parentAlias +
                    '_' +
                    joinColumn.referencedColumn!.propertyPath
                );
            })
                .join(' AND ');

            return (
                ' ' +
                joinAttr.direction +
                ' JOIN ' +
                this.getTableName(destinationTableName) +
                ' ' +
                this.escape(destinationTableAlias) +
                this.publicCreateTableLockExpression() +
                ' ON ' +
                this.customReplacePropertyNames(condition + appendedCondition)
            );
        } else {
            // means many-to-many
            const junctionTableName = relation.junctionEntityMetadata!.tablePath;

            const junctionAlias = joinAttr.junctionAlias;
            let junctionCondition = '',
                destinationCondition = '';

            if (relation.isOwning) {
                junctionCondition = relation.joinColumns
                    .map(joinColumn => {
                        // `post_category`.`postId` = `post`.`id`
                        return (
                            junctionAlias +
                            '.' +
                            joinColumn.propertyPath +
                            '=' +
                            parentAlias +
                            '_' +
                            joinColumn.referencedColumn!.propertyPath
                        );
                    })
                    .join(' AND ');

                destinationCondition = relation.inverseJoinColumns
                    .map(joinColumn => {
                        // `category`.`id` = `post_category`.`categoryId`
                        return (
                            destinationTableAlias +
                            '.' +
                            joinColumn.referencedColumn!.propertyPath +
                            '=' +
                            junctionAlias +
                            '.' +
                            joinColumn.propertyPath
                        );
                    })
                    .join(' AND ');
            } else {
                junctionCondition = relation
                    .inverseRelation!.inverseJoinColumns.map(joinColumn => {
                    // `post_category`.`categoryId` = `category`.`id`
                    return (
                        junctionAlias +
                        '.' +
                        joinColumn.propertyPath +
                        '=' +
                        parentAlias +
                        '_' +
                        joinColumn.referencedColumn!.propertyPath
                    );
                })
                    .join(' AND ');

                destinationCondition = relation
                    .inverseRelation!.joinColumns.map(joinColumn => {
                    // `post`.`id` = `post_category`.`postId`
                    return (
                        destinationTableAlias +
                        '.' +
                        joinColumn.referencedColumn!.propertyPath +
                        '=' +
                        junctionAlias +
                        '.' +
                        joinColumn.propertyPath
                    );
                })
                    .join(' AND ');
            }

            return (
                ' ' +
                joinAttr.direction +
                ' JOIN ' +
                this.getTableName(junctionTableName) +
                ' ' +
                this.escape(junctionAlias) +
                this.publicCreateTableLockExpression() +
                ' ON ' +
                this.customReplacePropertyNames(junctionCondition) +
                ' ' +
                joinAttr.direction +
                ' JOIN ' +
                this.getTableName(destinationTableName) +
                ' ' +
                this.escape(destinationTableAlias) +
                this.publicCreateTableLockExpression() +
                ' ON ' +
                this.customReplacePropertyNames(
                    destinationCondition + appendedCondition,
                )
            );
        }
    });

    return joins.join(' ');
};

SelectQueryBuilder.prototype.createMainEntityJoinExpression = function <Entity>(
    this: SelectQueryBuilder<Entity>,
): string {
    // examples:
    // select from owning side
    // qb.select("post")
    //     .leftJoinAndSelect("post.category", "category");
    // select from non-owning side
    // qb.select("category")
    //     .leftJoinAndSelect("category.post", "post");

    const joins = this.expressionMap.joinAttributes.map(joinAttr => {
        const relation = joinAttr.relation;
        const destinationTableName = joinAttr.tablePath;
        const destinationTableAlias = joinAttr.alias.name;
        let appendedCondition = joinAttr.condition
            ? ' AND (' + joinAttr.condition + ')'
            : '';
        const parentAlias = joinAttr.parentAlias;

        // if join was build without relation (e.g. without "post.category") then it means that we have direct
        // table to join, without junction table involved. This means we simply join direct table.
        if (!parentAlias || !relation) {
            const destinationJoin = joinAttr.alias.subQuery
                ? joinAttr.alias.subQuery
                : this.getTableName(destinationTableName);
            return (
                ' ' +
                joinAttr.direction +
                ' JOIN ' +
                destinationJoin +
                ' ' +
                this.escape(destinationTableAlias) +
                this.publicCreateTableLockExpression() +
                (joinAttr.condition
                    ? ' ON ' + this.customReplacePropertyNames(joinAttr.condition)
                    : '')
            );
        }

        // if real entity relation is involved
        if (relation.isManyToOne || relation.isOneToOneOwner) {
            // JOIN `category` `category` ON `category`.`id` = `post`.`categoryId`
            const condition = relation.joinColumns
                .map(joinColumn => {
                    let result =
                        parentAlias +
                        '.' +
                        relation.propertyPath +
                        '.' +
                        joinColumn.referencedColumn!.propertyPath;

                    if (parentAlias === this.expressionMap.mainAlias!.name) {
                        result = this.camelToSnakeCase(result);
                    }

                    return (
                        destinationTableAlias +
                        '.' +
                        joinColumn.referencedColumn!.propertyPath +
                        '=' +
                        result
                    );
                })
                .join(' AND ');

            return (
                ' ' +
                joinAttr.direction +
                ' JOIN ' +
                this.getTableName(destinationTableName) +
                ' ' +
                this.escape(destinationTableAlias) +
                this.publicCreateTableLockExpression() +
                ' ON ' +
                this.customReplacePropertyNames(condition + appendedCondition)
            );
        } else if (relation.isOneToMany || relation.isOneToOneNotOwner) {
            // JOIN `post` `post` ON `post`.`categoryId` = `category`.`id`
            const condition = relation
                .inverseRelation!.joinColumns.map(joinColumn => {
                if (
                    relation.inverseEntityMetadata.tableType === 'entity-child' &&
                    relation.inverseEntityMetadata.discriminatorColumn
                ) {
                    appendedCondition +=
                        ' AND ' +
                        destinationTableAlias +
                        '.' +
                        relation.inverseEntityMetadata.discriminatorColumn.databaseName +
                        "='" +
                        relation.inverseEntityMetadata.discriminatorValue +
                        "'";
                }

                return (
                    destinationTableAlias +
                    '.' +
                    relation.inverseRelation!.propertyPath +
                    '.' +
                    joinColumn.referencedColumn!.propertyPath +
                    '=' +
                    parentAlias +
                    '_' +
                    joinColumn.referencedColumn!.databaseName
                );
            })
                .join(' AND ');

            return (
                ' ' +
                joinAttr.direction +
                ' JOIN ' +
                this.getTableName(destinationTableName) +
                ' ' +
                this.escape(destinationTableAlias) +
                this.publicCreateTableLockExpression() +
                ' ON ' +
                this.customReplacePropertyNames(condition + appendedCondition)
            );
        } else {
            // means many-to-many
            const junctionTableName = relation.junctionEntityMetadata!.tablePath;

            const junctionAlias = joinAttr.junctionAlias;
            let junctionCondition = '',
                destinationCondition = '';

            if (relation.isOwning) {
                junctionCondition = relation.joinColumns
                    .map(joinColumn => {
                        // `post_category`.`postId` = `post`.`id`
                        return (
                            junctionAlias +
                            '.' +
                            joinColumn.propertyPath +
                            '=' +
                            parentAlias +
                            '_' +
                            joinColumn.referencedColumn!.propertyPath
                        );
                    })
                    .join(' AND ');

                destinationCondition = relation.inverseJoinColumns
                    .map(joinColumn => {
                        // `category`.`id` = `post_category`.`categoryId`
                        return (
                            destinationTableAlias +
                            '.' +
                            joinColumn.referencedColumn!.propertyPath +
                            '=' +
                            junctionAlias +
                            '.' +
                            joinColumn.propertyPath
                        );
                    })
                    .join(' AND ');
            } else {
                junctionCondition = relation
                    .inverseRelation!.inverseJoinColumns.map(joinColumn => {
                    // `post_category`.`categoryId` = `category`.`id`
                    return (
                        junctionAlias +
                        '.' +
                        joinColumn.propertyPath +
                        '=' +
                        parentAlias +
                        '_' +
                        joinColumn.referencedColumn!.propertyPath
                    );
                })
                    .join(' AND ');

                destinationCondition = relation
                    .inverseRelation!.joinColumns.map(joinColumn => {
                    // `post`.`id` = `post_category`.`postId`
                    return (
                        destinationTableAlias +
                        '.' +
                        joinColumn.referencedColumn!.propertyPath +
                        '=' +
                        junctionAlias +
                        '.' +
                        joinColumn.propertyPath
                    );
                })
                    .join(' AND ');
            }

            return (
                ' ' +
                joinAttr.direction +
                ' JOIN ' +
                this.getTableName(junctionTableName) +
                ' ' +
                this.escape(junctionAlias) +
                this.publicCreateTableLockExpression() +
                ' ON ' +
                this.customReplacePropertyNames(junctionCondition) +
                ' ' +
                joinAttr.direction +
                ' JOIN ' +
                this.getTableName(destinationTableName) +
                ' ' +
                this.escape(destinationTableAlias) +
                this.publicCreateTableLockExpression() +
                ' ON ' +
                this.customReplacePropertyNames(
                    destinationCondition + appendedCondition,
                )
            );
        }
    });

    return joins.join(' ');
};

SelectQueryBuilder.prototype.customCreateWhereExpression = function <Entity>(
    this: SelectQueryBuilder<Entity>,
    isInitialQuery: boolean,
): string {
    const conditionsArray = [];

    this.expressionMap.wheres.forEach(w => {
        if ((w.condition as any)?.condition?.length > 0) {
            (w.condition as any)?.condition.forEach(ww => {
                if (ww.condition.toString().indexOf('.') === -1) {
                    throw new TypeORMError(
                        'Cannot build query because main alias is not set. In your filters please use the alias.',
                    );
                }
            });
        } else if (w.condition.toString().indexOf('.') === -1) {
            throw new TypeORMError(
                'Cannot build query because main alias is not set. In your filters please use the alias.',
            );
        }
    });

    const whereExpression = this.customCreateWhereClausesExpression(
        this.expressionMap.wheres,
        isInitialQuery,
    );

    if (whereExpression.length > 0 && whereExpression !== '1=1') {
        conditionsArray.push(this.customReplacePropertyNames(whereExpression));
    }

    if (this.expressionMap.mainAlias!.hasMetadata) {
        const metadata = this.expressionMap.mainAlias!.metadata;
        // Adds the global condition of "non-deleted" for the entity with delete date columns in select query.
        if (
            this.expressionMap.queryType === 'select' &&
            !this.expressionMap.withDeleted &&
            metadata.deleteDateColumn
        ) {
            const column = this.expressionMap.aliasNamePrefixingEnabled
                ? this.expressionMap.mainAlias!.name +
                '.' +
                snakeCase(metadata.deleteDateColumn.propertyName)
                : snakeCase(metadata.deleteDateColumn.propertyName);

            const condition = `${this.customReplacePropertyNames(column)} IS NULL`;
            conditionsArray.push(condition);
        }

        if (metadata.discriminatorColumn && metadata.parentEntityMetadata) {
            const column = this.expressionMap.aliasNamePrefixingEnabled
                ? this.expressionMap.mainAlias!.name +
                '.' +
                metadata.discriminatorColumn.databaseName
                : metadata.discriminatorColumn.databaseName;

            if (column.indexOf('.') === -1) {
                throw new TypeORMError(
                    'Cannot build query because main alias is not set. In your Pipeline please use the alias.',
                );
            }

            const condition = `${this.customReplacePropertyNames(
                column,
            )} IN (:...discriminatorColumnValues)`;
            conditionsArray.push(condition);
        }
    }

    if (this.expressionMap.extraAppendedAndWhereCondition) {
        const condition = this.customReplacePropertyNames(
            this.expressionMap.extraAppendedAndWhereCondition,
        );
        if (condition.indexOf('.') === -1) {
            throw new TypeORMError(
                'Cannot build query because main alias is not set. In your filters please use the alias.',
            );
        }
        conditionsArray.push(condition);
    }

    if (!conditionsArray.length) {
        return '';
    } else if (conditionsArray.length === 1) {
        return this.findAndReplaceMainAliasInWhereExpression(
            isInitialQuery,
            ` WHERE ${conditionsArray[0]}`,
        );
    } else {
        return this.findAndReplaceMainAliasInWhereExpression(
            isInitialQuery,
            ` WHERE ( ${conditionsArray.join(' ) AND ( ')} )`,
        );
    }
};

SelectQueryBuilder.prototype.findAndReplaceMainAliasInWhereExpression = function <
    Entity
>(
    this: SelectQueryBuilder<Entity>,
    isInitialQuery: boolean,
    expression: string,
): string {
    if (isInitialQuery) {
        return expression;
    } else {
        const alias = this.expressionMap.mainAlias.name;
        return expression
            .split(`${alias}.`)
            .join(`${alias}_`)
            .split(`"${alias}"."`)
            .join(`"${alias}_`)
            .split(`"${alias}".`)
            .join(`"${alias}_`);
    }
};

SelectQueryBuilder.prototype.customCreateWhereClausesExpression = function <
    Entity
>(
    this: SelectQueryBuilder<Entity>,
    clauses: WhereClause[],
    skipNonMainAliases: boolean,
): string {
    const alternativeAliases = this.expressionMap.aliases
        .filter(x => x.name !== this.expressionMap.mainAlias.name)
        .map(x => x.name);

    return clauses
        .filter((clause, index) => {
            if (!skipNonMainAliases) return true;

            const expression = this.createWhereConditionExpression(clause.condition);
            let shouldSkip = false;

            alternativeAliases.forEach(x => {
                if (expression.indexOf(x) !== -1) {
                    shouldSkip = true;
                }
            });

            return !shouldSkip;
        })
        .map((clause, index) => {
            const expression = this.createWhereConditionExpression(clause.condition);

            switch (clause.type) {
                case 'and':
                    return (index > 0 ? 'AND ' : '') + expression;
                case 'or':
                    return (index > 0 ? 'OR ' : '') + expression;
            }

            return expression;
        })
        .join(' ')
        .trim();
};

SelectQueryBuilder.prototype.customGetMany = async function <Entity>(
    this: SelectQueryBuilder<Entity>,
): Promise<Entity[]> {
    if (this.expressionMap.lockMode === 'optimistic')
        throw new OptimisticLockCanNotBeUsedError();

    let results;

    if (this.initialQuery) {
        results = await this.customGetRawAndEntities();
    } else {
        results = await this.getRawAndEntities();
    }

    return results.entities;
};

SelectQueryBuilder.prototype.customGetRawAndEntities = async function <
    Entity,
    T = any
>(
    this: SelectQueryBuilder<Entity>,
): Promise<{
    entities: Entity[];
    raw: T[];
}> {
    const queryRunner = this.obtainQueryRunner();
    let transactionStartedByUs = false;
    try {
        // start transaction if it was enabled
        if (
            this.expressionMap.useTransaction === true &&
            queryRunner.isTransactionActive === false
        ) {
            await queryRunner.startTransaction();
            transactionStartedByUs = true;
        }

        this.expressionMap.queryEntity = true;
        const results = await this.customExecuteEntitiesAndRawResults(queryRunner);

        // close transaction if we started it
        if (transactionStartedByUs) {
            await queryRunner.commitTransaction();
        }

        return results;
    } catch (error) {
        // rollback transaction if we started it
        if (transactionStartedByUs) {
            try {
                await queryRunner.rollbackTransaction();
            } catch (rollbackError) {
            }
        }
        throw error;
    } finally {
        if (queryRunner !== this.queryRunner)
            // means we created our own query runner
            await queryRunner.release();
    }
};

// We bearely touch this function, we only modify the pagination enabled part, so for that I splited it into a new function @loadRawResultsPaginated
SelectQueryBuilder.prototype.customExecuteEntitiesAndRawResults = async function <
    Entity
>(
    this: SelectQueryBuilder<Entity>,
    queryRunner: QueryRunner,
): Promise<{ entities: Entity[]; raw: any[] }> {
    if (!this.expressionMap.mainAlias)
        throw new TypeORMError(
            `Alias is not set. Use "from" method to set an alias.`,
        );

    if (
        (this.expressionMap.lockMode === 'pessimistic_read' ||
            this.expressionMap.lockMode === 'pessimistic_write' ||
            this.expressionMap.lockMode === 'pessimistic_partial_write' ||
            this.expressionMap.lockMode === 'pessimistic_write_or_fail' ||
            this.expressionMap.lockMode === 'for_no_key_update' ||
            this.expressionMap.lockMode === 'for_key_share') &&
        !queryRunner.isTransactionActive
    )
        throw new PessimisticLockTransactionRequiredError();

    if (this.expressionMap.lockMode === 'optimistic') {
        const metadata = this.expressionMap.mainAlias.metadata;
        if (!metadata.versionColumn && !metadata.updateDateColumn)
            throw new NoVersionOrUpdateDateColumnError(metadata.name);
    }

    const relationIdLoader = new RelationIdLoader(
        this.connection,
        queryRunner,
        this.expressionMap.relationIdAttributes,
    );
    const relationCountLoader = new RelationCountLoader(
        this.connection,
        queryRunner,
        this.expressionMap.relationCountAttributes,
    );
    const relationIdMetadataTransformer = new RelationIdMetadataToAttributeTransformer(
        this.expressionMap,
    );
    relationIdMetadataTransformer.transform();
    const relationCountMetadataTransformer = new RelationCountMetadataToAttributeTransformer(
        this.expressionMap,
    );
    relationCountMetadataTransformer.transform();

    let rawResults: any[] = [],
        entities: any[] = [];

    // for pagination enabled (e.g. skip and take) its much more complicated - its a special process
    // where we make two queries to find the data we need
    // first query find ids in skip and take range
    // and second query loads the actual data in given ids range
    if (
        (this.expressionMap.offset || this.expressionMap.limit) &&
        this.expressionMap.joinAttributes.length > 0
    ) {
        // THIS IS THE ONLY OVERRIDE IN THIS FUNCTION
        rawResults = await this.loadRawResultsPaginated(queryRunner);
    } else {
        rawResults = await this.loadRawResults(queryRunner);
    }

    if (rawResults.length > 0) {
        // transform raw results into entities
        const rawRelationIdResults = await relationIdLoader.load(rawResults);
        const rawRelationCountResults = await relationCountLoader.load(rawResults);
        const transformer = new RawSqlResultsToEntityTransformer(
            this.expressionMap,
            this.connection.driver,
            rawRelationIdResults,
            rawRelationCountResults,
            this.queryRunner,
        );
        entities = transformer.transform(rawResults, this.expressionMap.mainAlias!);

        // broadcast all "after load" events
        if (
            this.expressionMap.callListeners === true &&
            this.expressionMap.mainAlias.hasMetadata
        ) {
            await queryRunner.broadcaster.broadcast(
                'Load',
                this.expressionMap.mainAlias.metadata,
                entities,
            );
        }
    }

    if (this.expressionMap.relationLoadStrategy === 'query') {
        await Promise.all(
            this.relationMetadatas.map(async relation => {
                const relationTarget = relation.inverseEntityMetadata.target;
                const relationAlias = relation.inverseEntityMetadata.targetName;

                const select = Array.isArray(this.findOptions.select)
                    ? OrmUtils.propertyPathsToTruthyObject(
                        this.findOptions.select as string[],
                    )
                    : this.findOptions.select;
                const relations = Array.isArray(this.findOptions.relations)
                    ? OrmUtils.propertyPathsToTruthyObject(this.findOptions.relations)
                    : this.findOptions.relations;

                const queryBuilder = this.createQueryBuilder()
                    .select(relationAlias)
                    .from(relationTarget, relationAlias)
                    .setFindOptions({
                        select: select
                            ? OrmUtils.deepValue(select, relation.propertyPath)
                            : undefined,
                        order: this.findOptions.order
                            ? OrmUtils.deepValue(
                                this.findOptions.order,
                                relation.propertyPath,
                            )
                            : undefined,
                        relations: relations
                            ? OrmUtils.deepValue(relations, relation.propertyPath)
                            : undefined,
                        withDeleted:
                            this.expressionMap?.parameters[
                            relationAlias + '_with_deleleted'
                                ] ?? this.findOptions.withDeleted,
                        relationLoadStrategy: this.findOptions.relationLoadStrategy,
                    });
                if (entities.length > 0) {
                    const relatedEntityGroups: any[] = await this.connection.relationIdLoader.loadManyToManyRelationIdsAndGroup(
                        relation,
                        entities,
                        undefined,
                        queryBuilder,
                    );
                    entities.forEach(entity => {
                        const relatedEntityGroup = relatedEntityGroups.find(
                            group => group.entity === entity,
                        );
                        if (relatedEntityGroup) {
                            const value =
                                relatedEntityGroup.related === undefined
                                    ? null
                                    : relatedEntityGroup.related;
                            relation.setEntityValue(entity, value);
                        }
                    });
                }
            }),
        );
    }

    return {
        raw: rawResults,
        entities: entities,
    };
};

SelectQueryBuilder.prototype.loadRawResultsPaginated = async function <Entity>(
    this: SelectQueryBuilder<Entity>,
    queryRunner: QueryRunner,
): Promise<Entity[]> {
    const metadata = this.expressionMap.mainAlias.metadata;
    const mainAliasName = this.expressionMap.mainAlias.name;

    let rawResults: any[] = await this.getDistinctIdsQuery().getRawMany();

    if (rawResults.length > 0) {
        let condition = '';
        const parameters: ObjectLiteral = {};
        if (metadata.hasMultiplePrimaryKeys) {
            condition = rawResults
                .map((result, index) => {
                    return metadata.primaryColumns
                        .map(primaryColumn => {
                            const paramKey = `orm_distinct_ids_${index}_${primaryColumn.databaseName}`;
                            parameters[paramKey] =
                                result[`ids_${mainAliasName}_${primaryColumn.databaseName}`];
                            return `${mainAliasName}.${primaryColumn.propertyPath}=:${paramKey}`;
                        })
                        .join(' AND ');
                })
                .join(' OR ');
        } else {
            const alias = DriverUtils.buildAlias(
                this.connection.driver,
                undefined,
                'ids_' + mainAliasName,
                metadata.primaryColumns[0].databaseName,
            );

            const ids = rawResults.map(result => result[alias]);
            const areAllNumbers = ids.every((id: any) => typeof id === 'number');
            if (areAllNumbers) {
                // fixes #190. if all numbers then its safe to perform query without parameter
                condition = `${mainAliasName}.${
                    metadata.primaryColumns[0].propertyPath
                } IN (${ids.join(', ')})`;
            } else {
                parameters['orm_distinct_ids'] = ids;
                condition =
                    mainAliasName +
                    '.' +
                    metadata.primaryColumns[0].propertyPath +
                    ' IN (:...orm_distinct_ids)';
            }
        }
        rawResults = await this.clone()
            .skip(undefined)
            .limit(undefined)
            .offset(undefined)
            .take(undefined)
            .mergeExpressionMap({
                extraAppendedAndWhereCondition: condition,
            })
            .setParameters(parameters)
            .loadRawResults(queryRunner);
    }

    return rawResults;
};

SelectQueryBuilder.prototype.getDistinctIdsQuery = function <Entity>(
    this: SelectQueryBuilder<Entity>,
): SelectQueryBuilder<any> {
    const [selects] = this.createOrderByCombinedWithSelectExpression(
        'distinctAlias',
    );
    const metadata = this.expressionMap.mainAlias.metadata;
    const mainAliasName = this.expressionMap.mainAlias.name;

    const querySelects = metadata.primaryColumns.map(primaryColumn => {
        const distinctAlias = this.escape('distinctAlias');
        const columnAlias = this.escape(
            DriverUtils.buildAlias(
                this.connection.driver,
                undefined,
                mainAliasName,
                primaryColumn.databaseName,
            ),
        );

        const alias = DriverUtils.buildAlias(
            this.connection.driver,
            undefined,
            'ids_' + mainAliasName,
            primaryColumn.databaseName,
        );

        return `${distinctAlias}.${columnAlias} AS ${this.escape(alias)}`;
    });

    return (
        this.connection
            .createQueryBuilder()
            .select(`DISTINCT ${querySelects.join(', ')}, row_number`)
            // .addSelect(selects)
            .addCommonTableExpression(this.initialQuery, 'withinitialentity')
            .addCommonTableExpression(this.mainQuery, 'withmainquery')
            .from(`withmainquery`, 'distinctAlias')
            .orderBy('row_number', 'ASC')
            .offset(this.expressionMap.skip || this.expressionMap.offset)
            .limit(this.expressionMap.take || this.expressionMap.limit)
            .cache(
                this.expressionMap.cache
                    ? this.expressionMap.cache
                    : this.expressionMap.cacheId,
                this.expressionMap.cacheDuration,
            )
            .setParameters(this.getParameters())
            .setNativeParameters(this.expressionMap.nativeParameters)
    );
};

SelectQueryBuilder.prototype.customReplacePropertyNames = function <Entity>(
    this: SelectQueryBuilder<Entity>,
    statement: any,
): string {
    return this.replacePropertyNames(
        statement.replaceAll('deletedAt', 'deleted_at'),
    );
};

SelectQueryBuilder.prototype.camelToSnakeCase = function <Entity>(
    this: SelectQueryBuilder<Entity>,
    str: string,
): string {
    return (
        str
            // @ts-ignore
            .replaceAll('"', '')
            .replaceAll('.', '_')
            .replace(/[A-Z]/g, letter => `_${letter.toLowerCase()}`)
    );
};

// @ts-ignore
SelectQueryBuilder.prototype.createOrderByCombinedWithSelectExpression = function <
    Entity
>(
    this: SelectQueryBuilder<Entity>,
    parentAlias: string,
): [string, OrderByCondition] {
    // if table has a default order then apply it
    const orderBys = this.expressionMap.allOrderBys;
    const selectString = Object.keys(orderBys)
        .map(orderCriteria => {
            if (orderCriteria.indexOf('.') !== -1) {
                const criteriaParts = orderCriteria.split('.');
                const aliasName = criteriaParts[0];
                const propertyPath = criteriaParts.slice(1).join('.');
                const alias = this.expressionMap.findAliasByName(aliasName);
                const column = alias.metadata.findColumnWithPropertyPath(propertyPath);
                return (
                    this.escape(parentAlias) +
                    '.' +
                    this.escape(
                        DriverUtils.buildAlias(
                            this.connection.driver,
                            undefined,
                            aliasName,
                            column?.databaseName ||
                            alias.metadata.primaryColumns[0].databaseName,
                        ),
                    )
                );
            } else {
                if (
                    this.expressionMap.selects.find(
                        select =>
                            select.selection === orderCriteria ||
                            select.aliasName === orderCriteria,
                    )
                )
                    return this.escape(parentAlias) + '.' + this.escape(orderCriteria);

                return '';
            }
        })
        .join(', ');

    const orderByObject: OrderByCondition = {};
    Object.keys(orderBys).forEach(orderCriteria => {
        if (orderCriteria.indexOf('.') !== -1) {
            const criteriaParts = orderCriteria.split('.');
            const aliasName = criteriaParts[0];
            const propertyPath = criteriaParts.slice(1).join('.');
            const alias = this.expressionMap.findAliasByName(aliasName);
            const column = alias.metadata.findColumnWithPropertyPath(propertyPath);
            orderByObject[
            this.escape(parentAlias) +
            '.' +
            this.escape(
                DriverUtils.buildAlias(
                    this.connection.driver,
                    undefined,
                    aliasName,
                    column?.databaseName ||
                    alias.metadata.primaryColumns[0].databaseName,
                ),
            )
                ] = orderBys[orderCriteria];
        } else {
            if (
                this.expressionMap.selects.find(
                    select =>
                        select.selection === orderCriteria ||
                        select.aliasName === orderCriteria,
                )
            ) {
                orderByObject[
                this.escape(parentAlias) + '.' + this.escape(orderCriteria)
                    ] = orderBys[orderCriteria];
            } else {
                orderByObject[orderCriteria] = orderBys[orderCriteria];
            }
        }
    });

    return [selectString, orderByObject];
};
