export interface UpdateResultModel<T> {
  result: boolean;
  entity: T;
}
