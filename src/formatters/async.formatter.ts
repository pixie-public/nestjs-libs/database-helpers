import { Operator } from "../pipelines";
import { Pagination } from "../paginator";


export class AsyncFormatter<I, O> extends Operator {
  constructor(private funcFormatter: (i: I) => O) {
    super();
  }

  async applyOperator(data: any): Promise<any[]> {
    const d = await data;
    const toDesconstruct = d instanceof Array ? null : d;
    const items = d instanceof Pagination ? d.items : d;
    return {
      ...(toDesconstruct as any),
      items: await Promise.all(items.map(await this.funcFormatter)),
    };
  }

  shouldHandle(): boolean {
    return true;
  }
}
