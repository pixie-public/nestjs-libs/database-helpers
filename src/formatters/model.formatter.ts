export abstract class ModelFormatter<T> {
  protected abstract format(item: T): any;

  formatItem<A>(item: T, formatterFunc?: (item: T) => A): A {
    if (!item) {
      return null;
    }

    return formatterFunc ? formatterFunc(item) : this.format(item);
  }

  formatCollection<A>(items: T[], formatterFunc?: (item: T) => A): A[] {
    return items && items.length > 0 ? items.map(x => this.formatItem<A>(x, formatterFunc)) : [];
  }
}
