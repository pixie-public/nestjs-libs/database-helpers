import { Repository, SelectQueryBuilder } from 'typeorm';
import { Operator } from "./operator";

export class AddSelectOperator extends Operator {
  constructor(
    private propsToSelect: string[] /*{
      [P in keyof Entity]?: FindOptionsWhereProperty<NonNullable<Entity[P]>>;
    },*/,
  ) {
    super();
  }

  applyOperator(passable: Repository<any> | SelectQueryBuilder<any>): any {
    const q = this.getQuery(passable);
    return q.addSelect(this.propsToSelect);
  }

  /**
   * This method is always called to ensure this operator should be
   * executed. By default checks if the is a query param by the
   * name given from @getRequestParamName() in the request.
   *
   * @return boolean
   */
  shouldHandle(): boolean {
    return true;
  }
}
