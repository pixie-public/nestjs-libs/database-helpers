import { Operator } from './operator';
import { Repository, SelectQueryBuilder } from 'typeorm';

export class ListOperator extends Operator {
  applyOperator(passable: Repository<any> | SelectQueryBuilder<any>): any {
    const q = this.getQuery(passable);
    return q.getMany();
  }

  /**
   * This method is always called to ensure this operator should be
   * executed. By default checks if the is a query param by the
   * name given from @getRequestParamName() in the request.
   *
   * @return boolean
   */
  shouldHandle(): boolean {
    return true;
  }
}
