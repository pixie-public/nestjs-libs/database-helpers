import { Handler } from '../handler';
import { Request as Req } from 'express';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { ParsedQs } from 'qs';

export abstract class Operator extends Handler {
  protected metadata: {
    request: Req;
  };

  handle(passable: any): any {
    if (!this.shouldHandle()) {
      return passable;
    }

    return this.applyOperator(passable);
  }

  abstract applyOperator(passable: any): any;

  shouldHandle() {
    return (
      this.getRequest() === undefined ||
      this.requestHas(this.getRequestParamName())
    );
  }

  getRequest(): Req {
    return this.metadata?.request;
  }

  getQueryParams(): ParsedQs {
    return this.metadata?.request?.query;
  }

  requestHas(prop: string): boolean {
    return (
      this.getQueryParams() !== undefined &&
      this.getQueryParams()[prop] !== undefined
    );
  }

  getQuery(
    passable: Repository<any> | SelectQueryBuilder<any>,
  ): SelectQueryBuilder<any> {
    return passable instanceof Repository
      ? (passable as Repository<any>).createQueryBuilder('q')
      : passable;
  }

  protected getRequestParamName(): string {
    const operatorName = this.constructor.name.replace('Operator', '');
    const snaked = operatorName.replace(
      /[A-Z]/g,
      letter => `_${letter.toLowerCase()}`,
    );
    return snaked.substr(1, snaked.length);
  }
}
