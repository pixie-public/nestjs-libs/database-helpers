import { Operator } from './operator';
import { Brackets, Repository, SelectQueryBuilder } from 'typeorm';
import { isString } from 'class-validator';
import * as moment from 'moment';

const _mappers={
  '|': 'or',
  ',': 'and'
}
export class SearchByPropertiesOperator extends Operator {
  private properties;
  private readonly requestParamName;
  private readonly matcher;
  private readonly collation: string

  constructor(
      properties: any[] | string,
      requestParam = 'search',
      matcher = 'ILIKE',
      collation?: string
  ) {
    super();

    if (isString(properties)) {
      const temp = properties.split(',');
      properties = temp.map(x => x.trim());
    }

    this.properties = properties.reverse();
    this.requestParamName = requestParam;
    this.matcher = matcher;
    this.collation = collation ? `COLLATE "${collation}"` : '';
  }

  applyOperator(passable: Repository<any> | SelectQueryBuilder<any>): any {
    const q = this.getQuery(passable);
    const searchTermsRaw = this.getRequest().query[this.getRequestParamName()];
    const searchTerms = (
        searchTermsRaw.toString().match(/[^,|]+/g) || []
    ).map(x => x.split(':'));

    const queryRegex = new RegExp(`[${Object.keys(_mappers).join()}]`, 'g');
    const queries = ['where']
        .concat(
            (searchTermsRaw.toString().match(queryRegex) || []).map(
                x => `${_mappers[x]}Where`,
            ),
        )
        .reverse();

    return q.andWhere(
        new Brackets(query => {
          for (let i = 0; i < searchTerms.length; i++) {
            const term = searchTerms[i];
            const props = this.properties.filter(
                x => (x?.requestField || x) === term[0],
            );
            if (props.length > 0) {
              const randomTerm = (Math.random() + 1).toString(36).substring(7);
              if (props instanceof Object && props[0].type === 'date') {
                const dates = term[1].split('-');
                const randomTerm2 = (Math.random() + 1).toString(36).substring(7);
                if (dates.length > 1) {
                  query[queries.pop()](
                      `${props[0]?.dbField ||
                      term[0]} between :${randomTerm} and :${randomTerm2}`,
                      {
                        [randomTerm]: `%${this.prepareDate(
                            dates[0],
                            props[0]?.dateFormat,
                        )}%`,
                        [randomTerm2]: `%${this.prepareDate(
                            dates[1],
                            props[0].dateFormat,
                        )}%`,
                      },
                  );
                } else {
                  query[queries.pop()](
                      `${props[0]?.dbField || term[0]} = :${randomTerm}`,
                      {
                        [randomTerm]: `%${this.prepareDate(
                            dates[0],
                            props[0]?.dateFormat,
                        )}%`,
                      },
                  );
                }
              } else if (props instanceof Object && props[0].type === 'number') {
                query[queries.pop()](
                    `${props[0]?.dbField || term[0]} ${props[0]?.matcher || '='} :${randomTerm}`,
                    {
                      [randomTerm]: `${term[1]}`,
                    },
                );
              }  else if (props instanceof Object && props[0].type === 'string') {
                query[queries.pop()](
                    `${props[0]?.dbField || term[0] + '::varchar'} ${props[0]?.matcher || 'ILIKE'} :${randomTerm} ${this.collation}`,
                    {
                      [randomTerm]: `${term[1]}`,
                    },
                );
              } else if (props instanceof Object && props[0].type === 'boolean') {
                query[queries.pop()](
                    `${props[0]?.dbField || term[0]} ${props[0]?.matcher || '='} :${randomTerm} ${this.collation}`,
                    {
                      [randomTerm]: `${term[1]}`,
                    },
                );
              } else {
                query[queries.pop()](
                    `${term[0]}::varchar ${this.matcher} :${randomTerm} ${this.collation}`,
                    {
                      [randomTerm]: `%${term[1]}%`,
                    },
                );
              }
            }
          }
        }),
    );
  }

  private prepareDate(epochMillis, format = 'YYYY-MM-DD') {
    return moment.unix(epochMillis / 1000).format(format);
  }

  protected getRequestParamName(): string {
    return this.requestParamName;
  }
}

