import { Operator } from './operator';
import { Repository, SelectQueryBuilder } from 'typeorm';

export class RawWhereSQLOperator extends Operator {
  private readonly sql;

  constructor(sql: string) {
    super();

    this.sql = sql;
  }

  applyOperator(passable: Repository<any> | SelectQueryBuilder<any>): any {
    const q = this.getQuery(passable);
    return q.andWhere(`${this.sql}`);
  }

  shouldHandle() {
    return true;
  }

}
