import { Operator } from './operator';
import { Repository, SelectQueryBuilder } from 'typeorm';

export class CustomSQLOperator extends Operator {
  private readonly property;
  private readonly value;

  constructor(property: string, value: any) {
    super();

    this.property = property;
    this.value = value;
  }

  applyOperator(passable: Repository<any> | SelectQueryBuilder<any>): any {
    const q = this.getQuery(passable);
    return q.andWhere(`${this.property} ${this.value}`);
  }

  shouldHandle() {
    return true;
  }

}
