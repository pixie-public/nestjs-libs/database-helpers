import { Operator } from './operator';
import { Brackets, Repository, SelectQueryBuilder } from 'typeorm';
import { Handler } from "../handler";

export class ConditionGroupOperator extends Operator {
  constructor(protected operators: Handler[]) {
    super();
  }

  applyOperator(passable: Repository<any> | SelectQueryBuilder<any>): any {
    const q = this.getQuery(passable);

    q.orWhere(
        new Brackets(query => {
          for (const operator of this.operators) {
            operator
                .withMetadata(this.metadata)
                .withPipeline(this.getPipeline())
                .handle(query);
          }
        }),
    );

    return q;
  }

  shouldHandle() {
    return true;
  }
}
