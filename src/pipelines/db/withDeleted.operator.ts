import { Repository, SelectQueryBuilder } from 'typeorm';
import { Operator } from "./operator";

export class WithDeletedOperator extends Operator {

  applyOperator(passable: Repository<any> | SelectQueryBuilder<any>): any {
    const q = this.getQuery(passable);
    return q.withDeleted();
  }

  shouldHandle(): boolean {
    return true;
  }

}
