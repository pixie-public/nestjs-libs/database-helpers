import { Operator } from './operator';
import { Repository, SelectQueryBuilder } from 'typeorm';

export class WhereInOperator extends Operator {
  private readonly property;
  private readonly requestParamName;

  constructor(property , requestParamName) {
    super();

    this.property = property;
    this.requestParamName =
      requestParamName !== null ? requestParamName : property;
  }

  applyOperator(passable: Repository<any> | SelectQueryBuilder<any>): any {
    const q = this.getQuery(passable);
    const searchTerms = this.getRequest().query[
      this.getRequestParamName()
    ].toString();
    const inValues = searchTerms.split(',');

    const paramsObject = {};
    paramsObject[this.getRequestParamName()] = inValues;

    return q.andWhere(`${this.property} IN (:...${this.getRequestParamName()})`, paramsObject);
  }

  protected getRequestParamName(): string {
    return this.requestParamName;
  }
}
