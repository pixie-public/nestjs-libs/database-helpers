import { Operator } from './operator';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { isNumber } from 'class-validator';
import { IPaginationOptions, paginate } from '../../paginator';

export class PaginateOperator extends Operator {
  /**
   * Get all the possible pagination combinations.
   *
   * @var array
   */
  private defaultPagination = 10;
  private paginationMax = 100;

  constructor(
      paginationMax = 100
  ) {
    super();
    this.paginationMax = paginationMax;
  }

  applyOperator(passable: Repository<any> | SelectQueryBuilder<any>): any {
    const q = this.getQuery(passable);
    return paginate<any>(q, {
      limit: this.getPerPage(),
      page: this.getUserPage(),
    } as IPaginationOptions);
  }

  /**
   * This method is always called to ensure this operator should be
   * executed. By default checks if the is a query param by the
   * name given from @getRequestParamName() in the request.
   *
   * @return boolean
   */
  shouldHandle(): boolean {
    return true;
  }

  /**
   * Matches the query string against the possible paginate size
   * combinations and returns the correct size. If none is
   * sent, first one will be used by default.
   *
   * @return mixed
   */
  protected getPerPage() {
    let perPage = this.getUserPerPage();

    if (!isNumber(perPage) || perPage > this.paginationMax) {
      perPage = this.defaultPagination;
    }

    return perPage;
  }

  protected getUserPerPage(): number {
    try {
      if (this.requestHas('per_page')) {
        return Number.parseInt(this.getQueryParams()['per_page'].toString());
      }
    } catch (e) {
      // Ignore, use default.
    }

    return this.defaultPagination;
  }

  protected getUserPage(): number {
    try {
      if (this.requestHas('page')) {
        const page = Number.parseInt(this.getQueryParams()['page'].toString());
        return page <= 0 ? 1 : page;
      }
    } catch (e) {
      // Ignore, use default.
    }

    return 1;
  }
}
