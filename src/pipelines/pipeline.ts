import { Handler } from './handler';

export class Pipeline {
  protected metadata: any = {};

  constructor(protected operators: Handler[]) {}

  execute(passable: any): any {
    for (const operator of this.operators) {
      passable = operator
        .withMetadata(this.metadata)
        .withPipeline(this)
        .handle(passable);
    }

    return passable;
  }

  with(prop: string, data: any) {
    this.metadata[prop] = data;

    return this;
  }
}
